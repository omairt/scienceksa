function s = epoch2date(varargin)
% Convert epoch (number of seconds since 1-1-1970) to date string
e = varargin{1};
if nargin == 1
    fmt = 30;
else
    fmt = varargin{2};
end
if isnan(e)
    s = '-';
else
    s = datestr(datenum(1970,1,1)+e/3600/24,fmt);
end

