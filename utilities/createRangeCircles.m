function circles = createRangeCircles(lat1,lon1,ranges)

for i = 1:length(ranges)
    for j = 1:361
        [lat2,lon2]= track1(lat1,lon1,ranges(i),j);
        circles(i).lat(j) = lat2;
        circles(i).lon(j) = lon2;
    end
end