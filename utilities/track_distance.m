function [meanDistError, meanMaslError, meanSogError, meanBrgError, brgError, maslError, tiltError, b2, radaLength,iStart1,iStop1,iStart2,iStop2] = track_distance(track1,track2, radarLat, radarLon)

meanDistError = NaN;
meanMaslError = NaN;
meanSogError  = NaN;
meanBrgError  = NaN;
brgError = NaN;
maslError = NaN;
tiltError = NaN;
b2 = NaN;
radaLength = NaN;
iStart1 = NaN;
iStop1  = NaN;

iStart2 = NaN;
iStop2  = NaN;

if (numel(track1.ts) > 1) && (numel(track2.ts) >1)
    
    % Start and stop times
    tStart1 = track1.tsi(1);
    tStop1  = track1.tsi(end);
    
    tStart2 = track2.tsi(1);
    tStop2  = track2.tsi(end);
    
    % Calculate distance on the time portion in common
    
    tStart = max([tStart1 tStart2]);
    tStop  = min([tStop1 tStop2]);
    
    if tStart <= tStop % time intersection exists
        iStart1 = find(tStart1:tStop1 == tStart);
        iStop1  = find(tStart1:tStop1 == tStop);
        iStart2 = find(tStart2:tStop2 == tStart);
        iStop2  = find(tStart2:tStop2 == tStop);
        lat1 = track1.lati(iStart1:iStop1);
        lon1 = track1.loni(iStart1:iStop1);
        masl1 = track1.masli(iStart1:iStop1);
        sog1 = track1.sogi(iStart1:iStop1);
        lat2 = track2.lati(iStart2:iStop2);
        lon2 = track2.loni(iStart2:iStop2);
        masl2 = track2.masli(iStart2:iStop2);
        sog2 = track2.sogi(iStart2:iStop2);

        [r,~]= latlon2rangebearing(lat1,lon1,lat2,lon2);
        
        meanDistError = mean(r);
%         meanGeoError = mean((track1.lati(iStart1:iStop1) - track2.lati(iStart2:iStop2)).^2 + ...
%             (track1.loni(iStart1:iStop1) - track2.loni(iStart2:iStop2)).^2);

        maslError = masl1 - masl2;
        meanMaslError = mean(abs(masl1 - masl2));
        meanSogError = mean(abs(sog1 - sog2));
        tiltError = rad2deg(atan(maslError./track2.dsti(iStart2:iStop2)));

        [~,b1]= latlon2rangebearing(radarLat,radarLon,lat1,lon1);
        [~,b2]= latlon2rangebearing(radarLat,radarLon,lat2,lon2);
        
%        brgError = abs(b1-b2); brgError(brgError>360) = brgError(brgError>360)-360;
        brgError = b1-b2;
        brgError(brgError>180) = brgError(brgError>180)-360;
        brgError(brgError<-180) = brgError(brgError<-180)+360;
        
        meanBrgError = mean(brgError);
        
        radaLength = numel(track2.lati);

    end   
end
