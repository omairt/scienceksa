function hmOut = heatmapN(varargin)
%function hmOut = heatmapN(x,y,z,xBins,yBins,zBins,hmIn)

% Number of heatmap dimensions
nDim = floor(nargin/2);
nBins = 1;
hmSize = [];
for i = 1:nDim
    hmData{i} = varargin{i};    % input data for the i-th dimension
    hmBin{i} = varargin{nDim+i};% heat map bins for the i-th dimension
    nBins = nBins * numel(hmBin{i});
    hmSize = [hmSize numel(hmBin{i})];
end

nPoints = length(hmData{i});

if isempty(varargin{end})
    hmOut = zeros(1,nBins);
else
    hmOut = varargin{end};
end

for l = 1:nPoints
    for i = 1:nDim
        indx(i) = nnz(hmData{i}(l)>=hmBin{i});
    end
    if all(indx~=0)
        indxLinear = indx(nDim)-1;
        for i = nDim-1:-1:1
            indxLinear = numel(hmBin{i}) * indxLinear + indx(i)-1;
        end
        indxLinear = indxLinear + 1;
        hmOut(indxLinear) = hmOut(indxLinear) +  1;
    end
end