function [range,bearing] = latlon2rangebearing(lat1,lon1,lat2,lon2)

% LATLON2RANGEBEARING
%
% Function LATLON2RANGEBEARING calculates the distance and the relative
% bearing
% between two points on earth, given their coordinates. The bearing is of
% point
% #2 with respect to point #1, and is measured clockwise from north. 
%
% INPUTS:
%           lat1:       latitude of point 1  [decimal degrees]
%           lon1:       longitude of point 1 [decimal degrees]
%           lat2:       latitude of point 2  [decimal degrees]
%           lon2:       longitude of point 2 [decimal degrees]
% OUTPUTS
%           range:      distance [m]
%           bearing:    relative bearing clockwis from north [decimal
%           degrees]
%
% Alberto Baldacci, NOVEMBER 14TH 2001



RE = 6366719.755;       % Earth radius [m]

lat1=lat1*pi/180;
lon1=lon1*pi/180;                          
lat2=lat2*pi/180;                           
lon2=lon2*pi/180;  

CN=sin(lat2).*sin(lat1)+cos(lat2).*cos(lat1).*cos(lon1-lon2); 
range = abs(RE*acos(CN)); 
AN=sin(lat2).*cos(lat1)-sin(lat1).*cos(lat2).*cos(lon1-lon2);   
SN=sin(lon1-lon2).*cos(lat2);

bearing = (pi+atan2(SN,-AN))*180/pi;  