function e = date2epoch(varargin)
% Convert date string to epoch (number of seconds since 1-1-1970)
% Input type 1 (nargin == 1, string)
% 20150824T073000
% Input type 2 (nargin == 6)
% 2015,8,24,7,30,0
% Input type 3 (nargin == 1, array)
% [2015 8 24 7 30 0] for example returned by Matlab command clock

if nargin == 1
    if ischar(varargin{1}) % 20150824T073000
        yyyy = str2double(varargin{1}(1:4));
        mm = str2double(varargin{1}(5:6));
        dd = str2double(varargin{1}(7:8));
        h = str2double(varargin{1}(10:11));
        m = str2double(varargin{1}(12:13));
        s = str2double(varargin{1}(14:15));
    else
        yyyy = varargin{1}(1);
        mm = varargin{1}(2);
        dd = varargin{1}(3);
        h = varargin{1}(4);
        m = varargin{1}(5);
        s = varargin{1}(6);
    end
else
    yyyy = varargin{1};
    mm = varargin{2};
    dd = varargin{3};
    h = varargin{4};
    m = varargin{5};
    s = varargin{6};   
end

e = (datenum(yyyy,mm,dd,h,m,s) - datenum(1970,1,1))*3600*24;


