function [lat2,lon2]= track1(lat1,lon1,dst,brg)

%R = 6366719.755; % Earth radius in metres
R = 6375305.954; % Earth radius in Jeddah

% Convert degrees to radians
lat1 = double(lat1*pi/180);
lon1 = double(lon1*pi/180);
brg  = double(brg*pi/180);

lat2 = asin(sin(lat1)*cos(dst/R) + cos(lat1)*sin(dst/R).*cos(brg));
lon2 = lon1 + atan2(sin(brg).*sin(dst/R)*cos(lat1),  cos(dst/R) - sin(lat1)*sin(lat2));

% Convert radians to degrees
lat2 = rad2deg(lat2);
lon2 = rad2deg(lon2);