function filesplit(fileName,nLines)
% FILESPLIT
% Splits a file into parts of nLines
fid = fopen(fileName,'r');
[aa,bb,cc] = fileparts(fileName);
subFileIndx = 0;
headerLine = fgets(fid);
firstLine = fgets(fid);
comaIndx = strfind(firstLine,',');
timeString = firstLine(comaIndx(1)+1:comaIndx(1)+19);
indx = strfind(timeString,':');
timeString(indx) = [];
while ~feof(fid)
    subFileIndx = subFileIndx +1;
    fileNameOut = fullfile(aa,[bb '-' timeString cc]);
    fidOut = fopen(fileNameOut,'wt');
    fprintf(fidOut,headerLine);
    fprintf(fidOut,firstLine);
    
    for i = 1:nLines
        line = fgets(fid);
        if i==1 || i==nLines
        end
        if line ~= -1
            fprintf(fidOut,line);
        end
    end
    firstLine = fgets(fid);
    if firstLine ~= -1
        comaIndx = strfind(firstLine,',');
        timeString = firstLine(comaIndx(1)+1:comaIndx(1)+19);
        indx = strfind(timeString,':');
        timeString(indx) = [];
    end
    
    fclose(fidOut);
end
fclose(fid);