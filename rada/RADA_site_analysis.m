function [tracks,rada,heatMap] = RADA_site_analysis(satImageFilename,trackFilename,site,location,azimuthCorrection,heatMapParameters,filterParameters)

% ********* GEOMETRY
GFYRADAGeometry

rada = extractGeometry(rada,site,location);
radaConnid = rada.connid;
indx = strfind(radaConnid,'-');
radaConnid(indx) = []; 

% ********* DEFINE GRID FOR HEAT MAP

binSize = heatMapParameters.binSize;
latBins = rada.lat - 400*binSize: binSize: rada.lat + 400*binSize;
lonBins = rada.lon - 400*binSize: binSize: rada.lon + 400*binSize;

% ******** READ SATELLITE IMAGE, CROP TO REGION OF INTEREST AND PLOT
[G,AAcrop,RA] = readSatellite(satImageFilename,latBins,lonBins);

hsat = figure;
pcolor(G.xcrop(1:2:end),G.ycrop(1:2:end),AAcrop(1:2:end,1:2:end))
shading flat
colormap gray
hold on
plot(rada.lon,rada.lat,'k*','MarkerSize',12,'LineWidth',2)
circles = createRangeCircles(rada.lat,rada.lon,5000:5000:20000);
for i = 1:length(circles)
    plot(circles(i).lon,circles(i).lat,'k','LineWidth',1)
end

for a = 0:15:355
[lat2,lon2]= track1(rada.lat,rada.lon,20000,a);
plot([rada.lon lon2], [rada.lat lat2],'k','LineWidth',1)
text(lon2,lat2,num2str(a),'FontSize', 14,'FontWeight','bold')
end

% ******** LOAD TRACKS
load(trackFilename,'tracks')

% ******** CORRECT AZIMUTH IF NEEDED
tracks = RADA_azimuth_correction(tracks, rada.connid, rada.lat, rada.lon, azimuthCorrection);

% ******** DECORATE TRACKS WITH STATISTICS AND MISSING PARAMETERS
tracks = decorateTracks(tracks,radaConnid,filterParameters);

% ******** APPLY FILTERS
tracks = applyFilters(tracks,filterParameters);

% ******** PLOT TRACKS
for i = 1:length(tracks)
    lon = tracks(i).lon;
    lat = tracks(i).lat;
    
%     if isfield(tracks,'connid')
%         switch tracks(i).connid
%             case 'rada-82'
                plot(lon,lat,'LineWidth',2)
%             case {'rada', 'rada-42','rada-42-1','rada-42-2'}
%                 plot(lon,lat,'LineWidth',2)
%         end
%     end
end
daspect([1 cos(rada.lat/180*pi) 1])


% ******** GENERATE HEATMAPS
heatMap = generateHeatMap(tracks,latBins,lonBins,radaConnid,heatMapParameters.nPointsHeatMap);

RB = imref2d(size(heatMap));
RB.XWorldLimits = RA.XWorldLimits;
RB.YWorldLimits = RA.YWorldLimits;
C = imfuse(AAcrop,RA,flipud(log(heatMap')),RB,'falsecolor','Scaling','independent','ColorChannels',[1 2 0]);
figure, imshow(C)
daspect([1 cos(rada.lat/180*pi) 1])
hold on

% ******** PLOT TRACKS AND SPLIT BY STARTING FROM HEAT MAP CLUSTERS OR NOT

figure,hold on
pcolor(G.xcrop(1:2:end),G.ycrop(1:2:end),AAcrop(1:2:end,1:2:end))
shading flat
colormap gray
grid on
for i = 1:length(tracks)
    % PLOT MAVLINK, ADS, RADA TRACKS
    if ~isempty(strfind(tracks(i).connid,'rada'))
            ilat = find(tracks(i).lat(1)<= latBins,1,'first');
            ilon = find(tracks(i).lon(1)<= lonBins,1,'first');
            if heatMap(ilon,ilat) >= heatMapParameters.heatMapThreshold
                plot(tracks(i).lon,tracks(i).lat,'r','LineWidth',2)
            else
                plot(tracks(i).lon,tracks(i).lat,'b','LineWidth',2)
            end
    end
end
daspect([1 cos(rada.lat/180*pi) 1])


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                          FUNCTIONS
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [G,AAcrop,RA] = readSatellite(satImageFilename,latBins,lonBins)
A = imread(satImageFilename);
AA = squeeze(A(:,:,1));
G= GEOTIFF_READ(satImageFilename);

% Crop the image based on selected area
x1 = find(G.x>=lonBins(1),1,'first');
x2 = find(G.x>=lonBins(end),1,'first');

y2 = find(G.y>latBins(1),1,'last');
y1 = find(G.y>latBins(end),1,'last');

AAcrop = AA(y1:y2,x1:x2);
G.xcrop = G.x(x1:x2);
G.ycrop = G.y(y1:y2);
RA = imref2d(size(AAcrop));

function tracks = decorateTracks(tracks,radaConnid,filterParameters)
if ~isfield(tracks(1),'connid')
    for i = 1:length(tracks)
        tracks(i).connid = radaConnid;
    end
end
for i = 1:length(tracks)
    if numel(tracks(i).lat)>1
        if isempty(tracks(i).sog)
            tracks(i).sog = sqrt(tracks(i).velocity_x.^2 + tracks(i).velocity_y.^2);
        end
        
        
        
        % CALCULATE A FEW TRACK STATISTICS
        brad = tracks(i).brg/180*pi;
        bradUnwrap = unwrap(brad);
        tracks(i).angularspeed = diff(bradUnwrap);
        tracks(i).angularspeed(end+1) = tracks(i).angularspeed(end);
        tracks(i).maxDeltaRange     = abs(max(tracks(i).dst) - min(tracks(i).dst));
        tracks(i).maxDeltaAzimuth   = abs(max(bradUnwrap)-min(bradUnwrap));
        tracks(i).diffRange         = diff(tracks(i).dst);
        tracks(i).diffAzimuth       = diff(bradUnwrap)*180/pi;
        tracks(i).stdRange          = std(diff(tracks(i).dst));
        tracks(i).stdAzimuth        = std(diff(bradUnwrap));
        diffSOG = [];
        
        tracks(i).angularspeed = 0;
        tracks(i).diffSOG = [];
        tracks(i).y = [];
        tracks(i).masly = [];
        
        
        diffSOG(1) = 0;
        for j = 2:numel(tracks(i).lat)
            diffSOG(j) = abs(tracks(i).sog(j)-tracks(i).sog(j-1));
        end
        
        tracks(i).diffSOG = diffSOG;
        tracks(i).y = cusum(diffSOG,filterParameters.cusum_w);        
    else
        tracks(i).angularspeed = 0;
        tracks(i).diffSOG = [];
        tracks(i).y = [];
        tracks(i).masly = [];
    end
end

function tracks = applyFilters(tracks,filterParameters)
% TODO: add filter on track length min
% Apply filter to all track fields (not only lon, lat, range, masl
for i = 1:length(tracks)
    
    indx1 = find(tracks(i).sog>=filterParameters.sog_max);
    indx2 = find(tracks(i).y>=filterParameters.cusum_thr);
    indx3 = find(tracks(i).masly>=filterParameters.masl_cusum_thr);
    indx4 = find(tracks(i).masl<filterParameters.masl_min);
    indx5 = find(tracks(i).sog<=filterParameters.sog_min);
    
    indx = union(indx1,indx2);
    indx = union(indx,indx3);
    indx = union(indx,indx4);
    indx = union(indx,indx5);
    
    tracks(i).lon(indx) = NaN;
    tracks(i).lat(indx) = NaN;
    tracks(i).range(indx) = NaN;
    tracks(i).masl(indx)= NaN;
    
end

function heatMap = generateHeatMap(tracks,latBins,lonBins,heatMapConnid,nPoints)
n = 0;
for i = 1:length(tracks)
    m = length(tracks(i).lon);
    if strcmp(tracks(i).connid,heatMapConnid)
        n = n + 1;
        if n == 1
            heatMap = heatmapN(tracks(i).lon(1:min(nPoints,m)),tracks(i).lat(1:min(nPoints,m)),lonBins,latBins,[]);
        else
            heatMap = heatmapN(tracks(i).lon(1:min(nPoints,m)),tracks(i).lat(1:min(nPoints,m)),lonBins,latBins,heatMap);
        end
    end
end
heatMap(heatMap==0) = NaN;
heatMap = reshape(heatMap,[numel(lonBins) numel(latBins)]);
