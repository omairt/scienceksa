rada(1).site = 'S01';
rada(1).location = 'Location1';
rada(1).colloquial = 'Villa A - Truss tower';
rada(1).model = 'RPS82';
rada(1).connid = 'rada-82';
rada(1).lat = 24.70012143;
rada(1).lon = 46.61218726;
rada(1).masl = 655.5;
rada(1).panel(1).heading = -16.48 + 8.47; % before calibration 20210318: 356+8.47; % Frederik 352;
rada(1).panel(2).heading = 71.89 + 8.47; % before calibration 20210318: 74+8.47;  % Frederik 83;
rada(1).panel(3).heading = 162.53 + 8.47; % before calibration 20210318: 165+8.47; % Frederik 168;
rada(1).panel(4).heading = -107.11 + 8.47; % before calibration 20210318: 250+8.47; % Frederik 274;
rada(1).txfreq = 3329000;
rada(1).colorcode = 'g';
rada(1).maxrange = 40000;

rada(4).site = 'S02';
rada(4).location = 'Location2';
rada(4).colloquial = 'Royal Court - Truss tower';
rada(4).model = 'RPS82';
rada(4).connid = 'rada-82';
rada(4).lat = 24.66457835; % Frederik  24.66460883;
rada(4).lon = 46.63973276; % Frederik  46.63971650;
rada(4).masl = 675.42;
rada(4).panel(1).heading = 259.23; % Frederik  232;
rada(4).panel(2).heading = 349.23; % Frederik  348;
rada(4).panel(3).heading = 80.53; % Frederik  79;
rada(4).panel(4).heading = 169.73; % Frederik  170;
rada(4).txfreq = 3349000;
rada(4).colorcode = 'r';
rada(4).maxrange = 40000;

rada(5).site = 'S02';
rada(5).location = 'Location4';
rada(5).colloquial = 'Shura Monopole';
rada(5).model = 'RPS42';
rada(5).connid = 'rada-42';
rada(5).lat = 24.66182561; % Fred 24.66181073;
rada(5).lon = 46.63630667; % Fred 46.63629915;
rada(5).masl=649.29;
rada(5).panel(1).heading = -45+144.61; % Fred  96;
rada(5).panel(2).heading = 45+144.61; % Fred 180;
rada(5).panel(3).heading = 135+144.61; % Fred 277;
rada(5).panel(4).heading = -135+144.61 ; % Fred 7;
rada(5).txfreq = 3309000;
rada(5).colorcode = 'r';
rada(5).maxrange = 10000;


rada(6).site = 'S03';
rada(6).location = 'Location1';
rada(6).colloquial = 'Truss Tower';
rada(6).model = 'RPS42';
rada(6).connid = 'rada-42';
rada(6).lat = 24.73732008; % Fred 24.73731544;
rada(6).lon = 46.56441902; % Fred 46.56439612;
rada(6).masl= 691.12;
rada(6).panel(1).heading = 135.17+351.18 -360; % Fred  120;
rada(6).panel(2).heading = 212.41+351.18 -360; % Fred  195;
rada(6).panel(3).heading = -59.70+351.18; % Fred  279;
rada(6).panel(4).heading =  30.53+351.18 -360; % Fred  17;
rada(6).txfreq = 3339000;
rada(6).colorcode = 'b';
rada(6).maxrange = 10000;
 
rada(7).site = 'S03';
rada(7).location = 'Location2';
rada(7).colloquial = 'Palm Tower';
rada(7).model = 'RPS82';
rada(7).connid = 'rada-82';
rada(7).lat = 24.73390960;
rada(7).lon = 46.56896060;
rada(7).masl = 696.8;
rada(7).panel(1).heading = 73.42 + 9.3;% Before calibration 20210318 85+9.3; % Frederik  93;
rada(7).panel(2).heading = 159.10+ 9.3;% Before calibration 20210318 157+9.3; % Frederik  165;
rada(7).panel(3).heading = -114.89+9.3 +360;% Before calibration 20210318 236+9.3; % Frederik  244;
rada(7).panel(4).heading = -18.82+9.3 +360;% Before calibration 20210318 -5+9.3; % Frederik 3;
rada(7).txfreq = 3379000;
rada(7).colorcode = 'b';
rada(7).maxrange = 40000;

rada(8).site = 'S04';
rada(8).location = 'Location1';
rada(8).colloquial = 'North Palm';
rada(8).model = 'RPS82';
rada(8).connid = 'rada-82';
rada(8).lat = 24.77810974; % Frederik 24.77808025;
rada(8).lon = 46.50221075; % Frederik 46.50221748;
rada(8).masl = 791.69; 
rada(8).panel(1).heading = 12; % Frederik  12;
rada(8).panel(2).heading = 100; % Frederik  116;
rada(8).panel(3).heading = 192; % Frederik  185;
rada(8).panel(4).heading = 277; % Frederik  260;
rada(8).txfreq = 3359000;
rada(8).colorcode = 'k';
rada(8).maxrange = 40000;

rada(10).site = 'S04';
rada(10).location = 'Location3';
rada(10).colloquial = 'East Palm';
rada(10).model = 'RPS42';
rada(10).connid = 'rada-42-1';
rada(10).lat = 24.80220627;
rada(10).lon = 46.53248149;
rada(10).masl = 721;
% rada(10).panel(1).heading = 45.3+353; % <-before calibration 
% rada(10).panel(2).heading = 160+353;  % <-before calibration 
% rada(10).panel(3).heading = -145+353; % <-before calibration 
% rada(10).panel(4).heading = -38+353 ; % <-before calibration 

rada(10).panel(1).heading = 50.8+353;   %< -after calibration ; % Frederik  38;
rada(10).panel(2).heading = 141.78+353; % <-after calibration;% Frederik  153;
rada(10).panel(3).heading = -128.34+353;% <-after calibration;; % Frederik  208;
rada(10).panel(4).heading = -39.51+353; % <-after calibration; % Frederik  315;
rada(10).txfreq = 3319000;
rada(10).colorcode = 'k';
rada(10).maxrange = 10000;

% rada(11).site = 'S05';
% rada(11).location = 'Location1';
% rada(11).colloquial = 'Rooftop';
% rada(11).model = 'RPS82';
% rada(11).connid = 'rada-82';
% rada(11).lat = 21.5384227; %21.53842294; % from hemisphere before 16/4/2021
% rada(11).lon = 39.1437049; %39.14370379; % from hemisphere before 16/4/2021
% rada(11).masl = 36.69;      % from hemisphere
% rada(11).panel(1).heading = 51.52+1.53;   % <-after second calibration 16/20 April 2021;
% rada(11).panel(2).heading = 140.78+1.53;  % <-after second calibration 16/20 April 2021;
% rada(11).panel(3).heading = -129.39+1.53; % <-after second calibration 16/20 April 2021;
% rada(11).panel(4).heading = -39.43+1.53 ; % <-after second calibration 16/20 April 2021;
% rada(11).panel(1).pitch = 22.25;   % <-after second calibration 16/20 April 2021;
% rada(11).panel(2).pitch = 19.84;  % <-after second calibration 16/20 April 2021;
% rada(11).panel(3).pitch = 20.33; % <-after second calibration 16/20 April 2021;
% rada(11).panel(4).pitch = 20.00 ; % <-after second calibration 16/20 April 2021;
% % rada(11).panel(1).heading = 51.5576+1.53; % <-after first calibration 1 April 2021;
% % rada(11).panel(2).heading = 140.49+1.53;  % <-after first calibration 1 April 2021;
% % rada(11).panel(3).heading = -129.80+1.53; % <-after first calibration 1 April 2021;
% % rada(11).panel(4).heading = -39.29+1.53 ; % <-after first calibration 1 April 2021;
% % rada(11).panel(1).pitch   = 22.24;   % <-after first calibration 1 April 2021;
% % rada(11).panel(2).pitch   = 19.94;  % <-after first calibration 1 April 2021;
% % rada(11).panel(3).pitch   = 20.37; % <-after first calibration 1 April 2021;
% % rada(11).panel(4).pitch   = 20.52 ; % <-after first calibration 1 April 2021;
% % rada(11).panel(1).heading = 50+1.53;   % <-before any calibrations;
% % rada(11).panel(2).heading = 140+1.53;  % <-before any calibrations;
% % rada(11).panel(3).heading = -130+1.53; % <-before any calibrations;
% % rada(11).panel(4).heading = -40+1.53 ; % <-before any calibrations;
% % rada(11).panel(1).pitch   = 20;   % <-before any calibrations;
% % rada(11).panel(2).pitch   = 20;  % <-before any calibrations;
% % rada(11).panel(3).pitch   = 20; % <-before any calibrations;
% % rada(11).panel(4).pitch   = 20 ; % <-before any calibrations;
% rada(11).txfreq = 3389000;
% rada(11).colorcode = 'k';
% rada(11).maxrange = 40000;

rada(21).site = 'M1';
rada(21).location = 'SAT1b';
rada(21).colloquial = 'reservoir';
rada(21).model = 'RPS42';
rada(21).connid = 'rada';
% rada(21).lat = 25.41583; % ABphone on 20 September 2021 on top of berm
% rada(21).lon = 46.53250; % ABphone on 20 September 2021
% rada(21).masl = 600;     % ABphone on 20 September 2021
rada(21).lat = 25.4147591733;   % Hemisphere - reservoir
rada(21).lon = 46.532388895;    % Hemisphere - reservoir
rada(21).masl = 597;            % Hemisphere - reservoir
rada(21).panel(1).heading = -45+66.42;     % from GenConfig.xml in bitbucket
rada(21).panel(2).heading = 45+66.42;      % from GenConfig.xml in bitbucket
rada(21).panel(3).heading = 135+66.42;     % from GenConfig.xml in bitbucket
rada(21).panel(4).heading = -135+66.42;   % from GenConfig.xml in bitbucket
rada(21).txfreq = 3359000;
rada(21).colorcode = 'k';
rada(21).maxrange = 15000;


rada(22).site = 'RPS82';
rada(22).location = 'SAT1b';
rada(22).colloquial = 'bermtop';
rada(22).model = 'RPS82';
rada(22).connid = 'rada-82';
rada(22).lat = 25.41446627; % Hemispheere - berm top
rada(22).lon = 46.5317661017; % Hemispheere - berm top
rada(22).masl = 610;        % Hemispheere - berm top
% rada(22).lat = 25.41409845; % ABphone on 20 September 2021 in reservoir after sept 23 aft redeployment
% rada(22).lon = 46.53243563; % ABphone on 20 September 2021
% rada(22).masl = 605;     % ABphone on 20 September 2021

rada(22).panel(1).heading = -45+66.42;     % from GenConfig.xml in bitbucket
rada(22).panel(2).heading = 45+66.42;      % from GenConfig.xml in bitbucket
rada(22).panel(3).heading = 135+66.42;     % from GenConfig.xml in bitbucket
rada(22).panel(4).heading = -135+66.42;   % from GenConfig.xml in bitbucket
rada(22).txfreq = 3359000;
rada(22).colorcode = 'k';
rada(22).maxrange = 15000;



% rada(22).site = 'M2';
% rada(22).location = 'Location1';
% rada(22).colloquial = 'Rooftop';
% rada(22).model = 'RPS42';
% rada(22).connid = 'rada';
% rada(22).lat = 25.4155603; % 25.41535948; % from hemisphere up to 11 April
% rada(22).lon = 46.5321999; %46.5322768; % from hemisphere
% rada(22).masl = 598; %594.40;      % from hemisphere
% rada(22).panel(1).heading = -45+247.81;   % <-before calibration;
% rada(22).panel(2).heading = 45+247.81;  % <-before calibration;
% rada(22).panel(3).heading = 135+247.81; % <-before calibration;
% rada(22).panel(4).heading = -135+247.81 ; % <-before calibration;
% rada(22).txfreq = 3349000;
% rada(22).colorcode = 'k';
% rada(22).maxrange = 20000;



% SRG airport
% lat =  24.944345
% lon =  46.582773

% Khashm Al Aan range
% lat = 24.857046; 24°51'26.9"N
% lon = 47.066850; 47°04'00.9"E

% Al Toqi range
% lat = 25.4156;
% lon = 46.5322;
% masl = 598;

