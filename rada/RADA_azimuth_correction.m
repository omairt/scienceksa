function tracksOut = RADA_azimuth_correction(tracksIn, radaConnid, radarLat, radarLon, rotationAngle)
tracksOut = tracksIn;
for i = 1:length(tracksOut)
    if strcmp(tracksOut(i).connid, radaConnid)
        tracksOut(i).brg = tracksOut(i).brg + rotationAngle;
        [lat2,lon2] = track1(radarLat,radarLon, tracksOut(i).dst, tracksOut(i).brg);
        tracksOut(i).lat = lat2;
        tracksOut(i).lon = lon2;
    end
end