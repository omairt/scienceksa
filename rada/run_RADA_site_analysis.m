addpath(genpath('../utilities'));

% ********* SATELLITE IMAGE (IF AVAILABLE)
%satImageFilename = '/Users/AB/Dropbox (MARSSians)/science KSA/SATELLITE-TOPOGRAPHY/AlToqi-satellite.tif';
satImageFilename = '/Users/AB/Dropbox (MARSSians)/science KSA/SATELLITE-TOPOGRAPHY/Riyadh-satellite.tif';

% ********* RADAR TRACKS
%trackFilename = '/Users/AB/WORK-DATA/CUAS/GFY/M1/SAT1b/recordings-20211004141353-flight2-cat2-rps42-rps82-longrange/recordings/track_recorder.mat';
trackFilename = '/Users/AB/Dropbox (MARSSians)/science KSA/RADA-DATA/GFY-S01/Site01_rada82_export_2021_11_04.mat';
site                = 'S01';
location            = 'Location1';

azimuthCorrection = 0;

heatMapParameters.nPointsHeatMap = Inf;
heatMapParameters.heatMapThreshold = 0.5;
heatMapParameters.binSize = 0.0005;

% ********* FILTER PARAMETERS
filterParameters.cusum_w                = 1;%1;
filterParameters.cusum_thr              = Inf;%20;%1000;%50;
filterParameters.masl_cusum_w           = 5;
filterParameters.masl_cusum_thr         = Inf;%;50;
filterParameters.sog_max                = Inf;%50;%100;
filterParameters.sog_min                = 0;
filterParameters.masl_min               = -Inf; %50
filterParameters.rcs_max                = Inf;%1 for CUAS %0.03 for DJI;%0.03; %0.03;
filterParameters.rcs_min                = -Inf;%0.05 for CUAS; 0 for DJI
filterParameters.max_positional_error   = Inf; %between 60 and 80 for MARSS UAS
filterParameters.track_length_min       = 10;%10;%100

[tracks,rada,heatMap] = ...
    RADA_site_analysis(satImageFilename,trackFilename,site,location,azimuthCorrection,heatMapParameters,filterParameters);