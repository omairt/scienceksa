function tracks = RADA_parser(filename, formatType, radarLat, radarLon, maxLines, trackTimeOrder, removeDuplicates)
%******** INPUTS
% filename
%           string containing the full path of the file containing the
%           data, usually in csv format
% formatType
%           number identifying the file format (depends on what generated
%           the file); see examples below
% radarLat
%           latitude of radar in decimal degrees
% radarLon
%           longitude of radar in decimal degrees
% maxLines
%           max number of file lines to be parsed (useful incase of very
%           large files); can be equal to Inf if we want to read the full
%           file
% trackTimeOrder
%           0: do not sort by time; 1: sort by time (default)
% removeDuplicates
%           0: do not remove duplicates; 1: remove duplicates
%
%******** OUTPUTS
% tracks
%           structure containing tracks
%
%******** EXAMPLES
%
%*** filename = '/Users/AB/WORK-DATA/CUAS/GFY/S2/track_recorder_filtered.csv';
%
%** Format 1: NiDAR recordings, tracks, including all sources like RADA,
%Aeroscope, mavlink, ADSB etc [track_recorder.csv]
% id	ts	tkid	tkref	class	connclass	connid	lat	lng	masl	hdg	cog	sog	vspeed	threat_val
% 268510,2020-09-24 09:27:12.641,88163733cf9f630b503462fe208836e6,483520,air,rada,rada,21.430683,39.208412,44.43,,171.1,18.2,,5
%
%** Format 2: deprecated
% id,ts,type,source,content
% 1,2020-09-15 07:49:36.141,rawPoint,rada,"{""mh"": 28.12, ""brg"": 251.9491150148, ""cog"": 35.7172444976, ""dst"": 5252.1204393369, ""rcs"": 0.0069150291, ""sog"": 13.3683394631, ""tkid"": ""b628363dd11ee7972e84c0aad2937f5d"", ""tkref"": ""32052"", ""connid"": ""rada"", ""connclass"": ""rada"", ""deviceClass"": ""radio""}"
% 56552,2020-09-15 08:35:31.168,rawPoint,aeroscope,"{""lat"": 21.4157758486, ""lng"": 39.1857181102, ""tkid"": ""5f327056952fddbbca6d1eb35dc75855"", ""tkref"": ""08RDE8UP01003J-pilot"", ""connid"": ""0QRDG9MR03ND4J"", ""connclass"": ""pilot"", ""deviceClass"": ""radio""}"
% 66935,2020-09-15 08:40:55.164,rawPoint,aeroscope,"{""mh"": 0, ""cog"": 28.2, ""lat"": 21.4157414711, ""lng"": 39.1857524877, ""sog"": 0, ""tkid"": ""ed8a3df359dc768609535c97811b3327"", ""tkref"": ""08RDE8UP01003J"", ""connid"": ""0QRDG9MR03ND4J"", ""connclass"": ""drone"", ""deviceClass"": ""radio""}"
%
%** Format 3 : NiDAR recordings, tracks, only RADA tracks, raw (incouding RCS and other parameters not in format 1)
% [rada.csv | rada82.csv | rada42.csv]
%id,ts,tkid,tkref,target_family,target_type,target_subtype,target_detailed_id,latitude,longitude,altitude,doppler_velocity,radar_cross_section,valid,params_ambiguity,tentatively_valid,associated_plot,high_priority,stt,nb_associated_plots,time_since_last_association,age,pos_x,pos_y,pos_z,velocity_x,velocity_y,velocity_z,pos_error_x,pos_error_y,pos_error_z,velocity_error_x,velocity_error_y,velocity_error_z
%1402836,2020-11-05 13:22:11.147,a4bdbfb62cf493949b2361000598cb5f,1,unclassified,0,0,0,                                               21.442296683799998646691165049560368061065673828125,39.2096011285999992423967341892421245574951171875,-50.37166595459999740569401183165609836578369140625,-15.420874595600000844797250465489923954010009765625,0.0793493539000000025485093146926374174654483795166015625,t,    f,               f,                f,              f,            t,  21,                 1037793082,                 1088574280,2638.05999999999994543031789362430572509765625,-4063.329999999999927240423858165740966796875,-75.409999999999996589394868351519107818603515625,-6.5,      14.019999999999999573674358543939888477325439453125,8.0600000000000004973799150320701301097869873046875,25.129920959500001487185727455653250217437744140625,16.500358581499998678054907941259443759918212890625,25.516220092800001140176391345448791980743408203125,6.406960964199999608581492793746292591094970703125,4.28849077220000030052915462874807417392730712890625,6.29433679579999960651548462919890880584716796875
% ^id    ^ts                     ^tkid                            ^tkref ^target_family,target_type,target_subtype,target_detailed_id,latitude,                                           longitude,                                        altitude,                                           doppler_velocity,                                    radar_cross_section,                                      valid,params_ambiguity,tentatively_valid,associated_plot,high_priority,stt,nb_associated_plots,time_since_last_association,age,       pos_x,                                         pos_y,                                        pos_z,                                            velocity_x,velocity_y,velocity_z,pos_error_x,pos_error_y,pos_error_z,velocity_error_x,velocity_error_y,velocity_error_z
%
%** Format 4: NiDAR track follow (only tracks followed by camera)
% id,camera,start,stop,tkid
% 21879,l3-2,2021-03-28 09:20:35.728+00,2021-03-28 09:21:03.541+00,48d420db68aa36aba91664d47111d974
%
%** Format 5: CSV files generated by JupyterLab
% ,tkid,tkref,ts,radar_cross_section,doppler_velocity,altitude,latitude,longitude,pos_x,pos_y,pos_z,velocity_x,velocity_y,velocity_z,pos_error_x,pos_error_y,pos_error_z,velocity_error_x,velocity_error_y,velocity_error_z
% 0,00157728b9ac4191a7ae9e54165838a2,138849,2021-08-26 07:42:37.250,0.7526105046272278,-60.24180221557617,1208.8248291015625,24.684741973876953,46.5716552734375,-1698.300048828125,4100.35986328125,544.1699829101562,-36.86000061035156,-79.63999938964844,-9.960000038146973,231.93275451660156,49.89003372192383,314.1882019042969,45.89486312866211,25.642168045043945,67.9751205444336
%   ˆtkid                            ^tkref ^ts                     ^RCS               ^doppler speed     ^altitude          ^latitude          ^longitude       ^pos_x             ^pos_y           ^pos_z            ^velocity_x        ^velocity_y        ^velocity_z        ^pos_error_x       ^pos_error_y      ^pos_error_z      ^vel_error_x      ^vel_error_y       ^vel_error_z
% format = '%*d%s%d%s%10.6f%10.6f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f%10.2f';
%
% Unified data format:
% 1. tkid
% 2. ts
% 3. class
% 4. connclass
% 5. lat
% 6. lon
% 7. masl
% 8. hdg
% 9. cog
% 10. sog
% 11. vspeed
% 12. threat_val
% 13. brg
% 14. dst
% 15. rcs
% 16. doppler
% 17. valid
% 18. pos_x
% 19. pos_y
% 20. pos_z
% 21. velocity_x
% 22. velocity_y
% 23. velocity_z
% 24. pos_error_x
% 25. pos_error_y
% 26. pos_error_z
% 27. velocity_error_x
% 28. velocity_error_y
% 29. velocity_error_z

addpath(genpath('../'))

% Script
fid = fopen(filename,'rt');
[aa,bb,cc] = fileparts(filename);

switch formatType
    case 1
        format = '%*d%s%s%s%s%s%s%10.6f%10.6f%10.2f%10.2f%10.2f%10.2f%10.2f%d';
    case 2
        format = '%*d%s%*s%s%s%s%s%s%s%s%s%s%*s%*s%*s';
    case 3
        format = '%*d%s%s%d%s%*d%*d%*d%75.70f%75.70f%75.70f%75.70f%75.70f%s%*s%*s%*s%*s%*s%*d%*d%*d%75.60f%75.60f%75.60f%75.60f%75.60f%75.60f%75.60f%75.60f%75.60f%75.60f%75.60f%75.60f';
    case 4
        format = '%*d%*s%*s%*s%s';
    case 5
        format = '%*d%s%d%s%30.20f%30.20f%30.20f%30.20f%30.20f%30.20f%30.20f%30.20f%30.20f%30.20f%30.20f%30.20f%30.20f%30.20f%30.20f%30.20f%30.20f';
        indx = strfind(bb,'_');
        connid = bb(indx(1)+1:indx(2)-1);
end

disp('Parsing file...')

C = textscan(fid,format,maxLines,'Delimiter',',','Headerlines',1);
removeIndex =[];
switch formatType
    case 1
        % Removing beacon tracks
        for i = 1:length(C{2})
            tkidLength(i) = length(C{2}{i});
        end
        removeIndex = find(tkidLength ~=32);
        for i = 1:14
            C{i}(removeIndex) = [];
        end
    case 2
        % Preserving RADA and AEROSCOPE tracks only
        for i = 1:length(C{1})
            if ~strcmp(C{2}{i},'rada') %&& ~strcmp(C{2}{i},'aeroscope')
                removeIndex(end+1) = i;
            end
        end
        for i = 1:10
            C{i}(removeIndex) = [];
        end
        for i = 1:length(C{1})
            D{1}{i}  = C{1}{i};
            D{2}{i}  = C{2}{i};
            if strcmp(C{2}{i},'rada')
                D{3}{i}  = str2double(C{3}{i}(10:end)); %mh, masl
                D{4}{i}  = str2double(C{4}{i}(9:end)); %brg
                D{5}{i}  = str2double(C{5}{i}(9:end)); %cog
                D{6}{i}  = str2double(C{6}{i}(9:end)); %dst
                D{7}{i}  = str2double(C{7}{i}(9:end)); %rcs
                D{8}{i}  = str2double(C{8}{i}(9:end)); %sog
                D{9}{i}  = C{9}{i}(13:end-2); %tkid
                D{10}{i} = C{10}{i}(14:end-2); %tkref
            else
                if strcmp(C{3}{i}(5:6),'mh')
                    D{3}{i}  = str2double(C{3}{i}(10:end)); %mh, masl
                    D{5}{i}  = str2double(C{4}{i}(9:end)); %cog
                    D{8}{i}  = str2double(C{7}{i}(9:end)); %sog
                    D{9}{i}  = C{8}{i}(13:end-2); %tkid
                    D{10}{i} = C{9}{i}(14:end-2); %tkref
                    D{11}{i} = str2double(C{5}{i}(9:end)); %lat
                    D{12}{i} = str2double(C{6}{i}(9:end)); %lon
                end
            end
        end
end
display('Reorganising data')
nPoints = length(C{1});
tracks = [];

switch formatType
    case 1
        tkidUnique = unique(C{2});
        tkidAll = [C{2}{:}];
    case 2
        tkidUnique = unique(D{9});
        tkidAll = [D{9}{:}];
    case 3
        tkidUnique = unique(C{2});
        tkidAll = [C{2}{:}];
    case 5
        tkidUnique = unique(C{1});
        tkidAll = [C{1}{:}];
end

h = waitbar(0,'Please wait...');
for i = 1:length(tkidUnique)
    waitbar(i/length(tkidUnique))
    indx = strfind(tkidAll, tkidUnique{i});
    
    indx = int32((indx-1)/32+1);
    
    switch formatType
        case 1
            tracks(i).tkid       = C{2}{indx(1)};
            for j = 1:length(indx)
                tracks(i).ts(j)  = date2epoch(datevec(C{1}{indx(j)}(1:19)));
            end
            tracks(i).ts         = tracks(i).ts';
            tracks(i).class      = C{4}{indx(1)};
            tracks(i).connclass  = C{5}{indx(1)};
            tracks(i).connid     = C{6}{indx(1)};
            tracks(i).lat        = C{7}(indx);
            tracks(i).lon        = C{8}(indx);
            tracks(i).masl       = C{9}(indx);
            tracks(i).hdg        = C{10}(indx);
            tracks(i).cog        = C{11}(indx);
            tracks(i).sog        = C{12}(indx);
            tracks(i).vspeed     = C{13}(indx);
            tracks(i).threat_val = C{14}(indx);
            % Calculate brg and dst fields
            [r,b]= latlon2rangebearing(radarLat,radarLon,tracks(i).lat,tracks(i).lon);
            tracks(i).brg        = b;
            tracks(i).dst        = r;
            tracks(i).rcs        = [];
                        
        case 2
            tracks(i).tkid       = D{9}{indx(1)};
            for j = 1:length(indx)
                tracks(i).ts(j)  = date2epoch(datevec(D{1}{indx(j)}));
            end
            tracks(i).class      = [];
            tracks(i).connclass  = D{2}{indx(1)};
            if strcmp(tracks(i).connclass,'aeroscope')
                tracks(i).lat        = [D{11}{indx}];
                tracks(i).lon        = [D{12}{indx}];
            else
                tracks(i).connid     = C{6}{indx(1)};
                tracks(i).masl       = [D{3}{indx}];
                tracks(i).hdg        = [];
                tracks(i).cog        = [D{5}{indx}];
                tracks(i).sog        = [D{8}{indx}];
                tracks(i).vspeed     = [];
                tracks(i).threat_val = [];
                tracks(i).brg        = [D{4}{indx}];
                tracks(i).dst        = [D{6}{indx}];
                tracks(i).rcs        = [D{7}{indx}];
                [lat2,lon2]= track1(radarLat,radarLon,tracks(i).dst,tracks(i).brg);
                tracks(i).lat        = lat2;
                tracks(i).lon        = lon2;
            end
            
        case 3
            tracks(i).tkid       = C{2}{indx(1)};
            for j = 1:length(indx)
                tracks(i).ts(j)  = date2epoch(datevec(C{1}{indx(j)}));
            end
            tracks(i).ts         = tracks(i).ts';
            tracks(i).class      = 'rada';
            tracks(i).connclass  = 'rada';
            tracks(i).connid     = connid;
            tracks(i).lat        = C{5}(indx);
            tracks(i).lon        = C{6}(indx);
            tracks(i).masl       = C{7}(indx); % - radarMasl;
            tracks(i).hdg        = [];
            tracks(i).cog        = [];
            tracks(i).vspeed     = [];
            tracks(i).threat_val = [];
            % Calculate brg and dst fields
            [r,b]= latlon2rangebearing(radarLat,radarLon,tracks(i).lat,tracks(i).lon);
            tracks(i).brg        = b;
            tracks(i).dst        = r;
            tracks(i).rcs        = C{9}(indx);
            tracks(i).doppler    = C{8}(indx);
            tracks(i).valid      = C{10}(indx);
            
            tracks(i).pos_x      = C{11}(indx);
            tracks(i).pos_y      = C{12}(indx);
            tracks(i).pos_z      = C{13}(indx);
            if length(tracks(i).lat)>1
                lat = tracks(i).lat;
                lon = tracks(i).lon;
                [r,b] = latlon2rangebearing(lat(1:end-1),lon(1:end-1),lat(2:end),lon(2:end));
                tracks(i).cog = b;
                tracks(i).cog(end+1) = tracks(i).cog(end);
            end
            tracks(i).velocity_x = C{14}(indx);
            tracks(i).velocity_y = C{15}(indx);
            
            tracks(i).velocity_z = C{16}(indx);
            tracks(i).sog        = sqrt(tracks(i).velocity_x.^2 + tracks(i).velocity_y.^2);
            tracks(i).pos_error_x = C{17}(indx);
            tracks(i).pos_error_y = C{18}(indx);
            tracks(i).pos_error_z = C{19}(indx);
            tracks(i).velocity_error_x = C{20}(indx);
            tracks(i).velocity_error_y = C{21}(indx);
            tracks(i).velocity_error_z = C{22}(indx);
            
        case 5            
            tracks(i).tkid       = C{1}{indx(1)};
            for j = 1:length(indx)
                tracks(i).ts(j)  = date2epoch(datevec(C{3}{indx(j)}));
            end
            tracks(i).ts         = tracks(i).ts';
            tracks(i).class      = 'rada';
            tracks(i).connclass  = 'rada';
            tracks(i).connid     = connid;
            tracks(i).lat        = C{7}(indx);
            tracks(i).lon        = C{8}(indx);
            tracks(i).masl       = C{6}(indx); % - radarMasl;
            tracks(i).hdg        = [];
            tracks(i).cog        = [];
            tracks(i).vspeed     = [];
            tracks(i).threat_val = [];
            % Calculate brg and dst fields
            [r,b]= latlon2rangebearing(radarLat,radarLon,tracks(i).lat,tracks(i).lon);
            tracks(i).brg        = b;
            tracks(i).dst        = r;            
            tracks(i).rcs        = C{4}(indx);
            tracks(i).doppler    = C{5}(indx);
            tracks(i).valid      = [];
            
            tracks(i).pos_x      = C{9}(indx);
            tracks(i).pos_y      = C{10}(indx);
            tracks(i).pos_z      = C{11}(indx);
            if length(tracks(i).lat)>1
                lat = tracks(i).lat;
                lon = tracks(i).lon;
                [r,b] = latlon2rangebearing(lat(1:end-1),lon(1:end-1),lat(2:end),lon(2:end));
                tracks(i).cog = b;
                tracks(i).cog(end+1) = tracks(i).cog(end);
            end
            tracks(i).velocity_x = C{12}(indx);
            tracks(i).velocity_y = C{13}(indx);
            
            tracks(i).velocity_z = C{14}(indx);
            tracks(i).sog        = sqrt(tracks(i).velocity_x.^2 + tracks(i).velocity_y.^2);
            tracks(i).pos_error_x = C{15}(indx);
            tracks(i).pos_error_y = C{16}(indx);
            tracks(i).pos_error_z = C{17}(indx);
            tracks(i).velocity_error_x = C{18}(indx);
            tracks(i).velocity_error_y = C{19}(indx);
            tracks(i).velocity_error_z = C{20}(indx);
            
    end
    
    if removeDuplicates
        [c,ia] = unique(tracks(i).ts);
        if length(ia)~=length(tracks(i).ts)
            tracks(i).ts = tracks(i).ts(ia);
            tracks(i).lat = tracks(i).lat(ia);
            tracks(i).lon = tracks(i).lon(ia);
            tracks(i).masl = tracks(i).masl(ia);
            if formatType == 1
                tracks(i).hdg = tracks(i).hdg(ia);
                tracks(i).vspeed = tracks(i).vspeed(ia);
            end
            tracks(i).cog = tracks(i).cog(ia);
            tracks(i).sog = tracks(i).sog(ia);
            tracks(i).brg = tracks(i).brg(ia);
            tracks(i).dst = tracks(i).dst(ia);
        end
    end
    
    % Sort by time
    if trackTimeOrder
        [BB,II]= sort(tracks(i).ts,'ascend');
        tracks(i).ts = tracks(i).ts(II);
        tracks(i).lat        = tracks(i).lat(II);
        tracks(i).lon        = tracks(i).lon(II);
        tracks(i).masl       = tracks(i).masl(II);
        switch formatType
            case 1
                tracks(i).hdg        = tracks(i).hdg(II);
                tracks(i).vspeed     = tracks(i).vspeed(II);
                tracks(i).threat_val = tracks(i).threat_val(II);
                tracks(i).cog        = tracks(i).cog(II);
                tracks(i).sog        = tracks(i).sog(II);
            case 2
                tracks(i).brg        = tracks(i).brg(II);
                tracks(i).dst        = tracks(i).dst(II);
                tracks(i).rcs        = tracks(i).rcs(II);
                tracks(i).cog        = tracks(i).cog(II);
                tracks(i).sog        = tracks(i).sog(II);
            case 3
                tracks(i).brg               = tracks(i).brg(II);
                tracks(i).dst               = tracks(i).dst(II);
                tracks(i).sog               = tracks(i).sog(II);
                tracks(i).rcs               = tracks(i).rcs(II);
                tracks(i).doppler           = tracks(i).doppler(II);
                tracks(i).valid             = tracks(i).valid(II);
                tracks(i).velocity_x        = tracks(i).velocity_x(II);
                tracks(i).velocity_y        = tracks(i).velocity_y(II);
                tracks(i).velocity_z        = tracks(i).velocity_z(II);
                tracks(i).pos_error_x       = tracks(i).pos_error_x(II);
                tracks(i).pos_error_y       = tracks(i).pos_error_y(II);
                tracks(i).pos_error_z       = tracks(i).pos_error_z(II);
                tracks(i).velocity_error_x  = tracks(i).velocity_error_x(II);
                tracks(i).velocity_error_y  = tracks(i).velocity_error_y(II);
                tracks(i).velocity_error_z  = tracks(i).velocity_error_z(II);
                
        end
    end
    
    
end
display(['Data time window: ' C{1}{i} ' to ' C{1}{end}])
fclose(fid);

%****** SAVE TRACKS TO FILE 
save(fullfile(aa,bb),'tracks')

%****** PLOT TRACKS
figure,hold on
for i = 1:length(tracks)
plot(tracks(i).lon,tracks(i).lat,'LineWidth',2)
end