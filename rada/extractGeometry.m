function radaSelect = extractGeometry(rada,site,location)
for i = 1:length(rada)
    if strcmp(rada(i).site,site) && strcmp(rada(i).location,location)
        radaSelect = rada(i);
    end
end

radaSelect.rotationAngle = 0;

for j = 1:length(radaSelect.panel)
    if radaSelect.panel(j).heading>360
        radaSelect.panel(j).heading =   radaSelect.panel(j).heading - 360;
    end
    if radaSelect.panel(j).heading<0
        radaSelect.panel(j).heading =   radaSelect.panel(j).heading + 360;
    end
    
    [radaSelect.panel(j).endLat,radaSelect.panel(j).endLon] = track1(radaSelect.lat,radaSelect.lon,radaSelect.maxrange,radaSelect.panel(j).heading);
end
