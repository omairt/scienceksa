function [tracks,rada,trackFilename,site,location,mavlinkTrackIndex, radaTrackIndex] = RADA_drone_analysis(satelliteImageFilename,trackLeg,trackFilename, site, location, titleString, rotationAngle)
%******** INPUTS
% satelliteImageFilename
%           string containing the full path of the satellite image (.fig file)
%           example: '/Users/AB/Dropbox (MARSSians)/science KSA/SATELLITE-TOPOGRAPHY/AlToqi-satellite-map-with-markers.fig'
%
% trackLeg
%           string defining which drone track leg to calculate the
%           statistics for ['all'| 'inbound' | 'other']
%
% trackFilename
%           string containing full path of .mat file containing tracks
%           parsed by Matlab already
%           eample: '/Users/AB/Dropbox (MARSSians)/science KSA/RADA-DATA/AL-TOQI/recordings-20211004141353-flight2-cat2-rps42-rps82-longrange/recordings/track_recorder.mat'
%
% site
%           string indicating site; example: 'S01' | 'M1'
%
% location
%           string indicating the actual location within the site
%           example: 'Sat1b
%
% titleString
%           string containing the title for all plots
%           example: 'RPS82 - long range run
%
% rotationAngle
%           number defining rotation to apply to radar data if required
%           example: -85 or -36


% ********* LOAD SATELLITE IMAGE
uiopen(satelliteImageFilename,1)

% ********* GEOMETRY
GFYRADAGeometry

rada = extractGeometry(rada,site,location);

% ********* FILTER PARAMETERS
filterParameters.cusum_w                = 1;%1;
filterParameters.cusum_thr              = Inf;%20;%1000;%50;
filterParameters.masl_cusum_w           = 5;
filterParameters.masl_cusum_thr         = Inf;%;50;
filterParameters.sog_max                = Inf;%50;%100;
filterParameters.sog_min                = 0;
filterParameters.masl_min               = -Inf; %50
filterParameters.rcs_max                = Inf;%1 for CUAS %0.03 for DJI;%0.03; %0.03;
filterParameters.rcs_min                = -Inf;%0.05 for CUAS; 0 for DJI
filterParameters.max_positional_error   = Inf; %between 60 and 80 for MARSS UAS
filterParameters.track_length_min       = 10;%10;%100

% ******** LOAD TRACKS
load(trackFilename,'tracks')

% ******** APPLY AZIMUTH CORRECTION
tracks = RADA_azimuth_correction(tracks, rada.connid, rada.lat, rada.lon, rotationAngle);

% ******** DECORATE TRACKS WITH STATISTICS AND MISSING PARAMETERS
tracks = decorateTracks(tracks,rada.connid,filterParameters);

% ******** APPLY FILTERS
tracks = applyFilters(tracks,filterParameters);

%******************* Estract tracks associated to MAVLINK
% Associate rada tracks to MAVLINK (reference) tracks
refConnid = 'mavlink';
maslMin = 600;
sogMin  = 20;
distThr = 400; % track to track association distance [m]
distSog = 20; % track to track association SOG difference [m]
dt = 1; %interpolation interval, [sec]




%******** PLOT TRACKS
for i = 1:length(tracks)
    % PLOT MAVLINK, ADS, RADA TRACKS
    switch tracks(i).connclass
        case 'mavlink'
            track_length_min = 10;
            if length(tracks(i).lon)>track_length_min
                plot(tracks(i).lon,tracks(i).lat,'k:','LineWidth',3)
            end
        case 'drone'
            track_length_min = 10;
            if length(tracks(i).lon)>track_length_min
                plot(tracks(i).lon,tracks(i).lat,'c:','LineWidth',3)
            end
            
        case {'rada','rada-42','ads'}
            lon = tracks(i).lon;
            lat = tracks(i).lat;
            
            if isfield(tracks,'connid')
                switch tracks(i).connid
                    case 'pingStation'
                        plot(lon,lat,'k','LineWidth',1)
                    case 'rada-82'
                        plot(lon,lat,'r','LineWidth',1)
                    case {'rada', 'rada-42','rada-42-1','rada-42-2'}
                        plot(lon,lat,'b','LineWidth',1)
                end
            else
                plot(lon,lat,'LineWidth',2)
                %                         text(lon(1),lat(1),[num2str(tracks(i).tkref) '->'])
                %                         text(lon(end),lat(end),['<-' num2str(tracks(i).tkref)])
            end
    end
end


for i = 1:length(tracks)
    % Plot first point of each track
    switch tracks(i).connclass
        case {'rada','rada-42','ads'}
            lon = tracks(i).lon;
            lat = tracks(i).lat;
            
            if isfield(tracks,'connid')
                switch tracks(i).connid
                    %                         case 'rada-82'
                    %                             plot(lon,lat,'r','LineWidth',1)
                    case {'rada', 'rada-42','rada-42-1','rada-42-2'}
                        plot(lon(1),lat(1),'r.','LineWidth',1)
                end
            end
    end
end

% ******* Associate tracks
if contains(trackFilename,'rada')
    disp('Choose best zoom to start picking tracks')
    pause
    mavlinkTrackIndex = [];
    radaTrackIndex = pickTrackFcn(tracks);
else
    [mavlinkTrackIndex, radaTrackIndex, matchID] = mavlinkAssociation(tracks,rada,dt,refConnid,maslMin,sogMin,distThr,distSog);
end



% Plot associated tracks
for i = 1:length(radaTrackIndex)
    id = radaTrackIndex(i);
    plot(tracks(id).lon,tracks(id).lat,'LineWidth',3)
    plot(tracks(id).lon(1),tracks(id).lat(1),'r*','MarkerSize',12,'LineWidth', 3)
end


%****** Extract data of interest (inbound, other, all)
for i = 1:length(radaTrackIndex)
    switch trackLeg
        case 'inbound'
            if contains(trackFilename,'rada')
                indx = find(tracks(radaTrackIndex(i)).doppler > 20);
            else
                indx = find(tracks(radaTrackIndex(i)).cog < 270);
            end
        case 'outbound'
            if contains(trackFilename,'rada')
                indx = find(tracks(radaTrackIndex(i)).doppler < -20);
            else
                indx = find(tracks(radaTrackIndex(i)).cog > 270);
            end
        case 'all'
            indx = [];
    end
    tracks(radaTrackIndex(i)).lon(indx) = NaN;
    tracks(radaTrackIndex(i)).lat(indx) = NaN;
    tracks(radaTrackIndex(i)).masl(indx) = NaN;
    tracks(radaTrackIndex(i)).sog(indx) = NaN;
    tracks(radaTrackIndex(i)).cog(indx) = NaN;
    tracks(radaTrackIndex(i)).dst(indx) = NaN;
    if ~isempty(tracks(radaTrackIndex(i)).rcs)
        tracks(radaTrackIndex(i)).rcs(indx) = NaN;
        tracks(radaTrackIndex(i)).doppler(indx) = NaN;
    end
    
end


%***** RCS
RCStotal = [];
if contains(trackFilename,'rada')
    figure,hold on, grid
    for i = 1:length(radaTrackIndex)
        plot(tracks(radaTrackIndex(i)).ts,tracks(radaTrackIndex(i)).rcs,'LineWidth',2)
        RCStotal = [RCStotal;tracks(radaTrackIndex(i)).rcs];
    end
    xlabel('Time sample'),ylabel('RCS')
end
figure,hist(RCStotal,0:0.001:1)
xlabel('RCS'),ylabel('counts')



%***** SOG
figure,hold on, grid
for i = 1:length(mavlinkTrackIndex)
    plot(tracks(mavlinkTrackIndex(i)).ts,tracks(mavlinkTrackIndex(i)).sog,'k:','LineWidth',2)
end
for i = 1:length(radaTrackIndex)
    plot(tracks(radaTrackIndex(i)).ts,tracks(radaTrackIndex(i)).sog,'LineWidth',2)
end
xlabel('Time sample'),ylabel('Speed Over Ground (m/s)')

%***** COG
figure, hold on, grid
for i = 1:length(radaTrackIndex)
    plot(tracks(radaTrackIndex(i)).ts,tracks(radaTrackIndex(i)).cog,'LineWidth',2)
end
xlabel('Time Sample'), ylabel('COG [deg]')


%***** MASL
figure,hold on, grid
for i = 1:length(mavlinkTrackIndex)
    plot(tracks(mavlinkTrackIndex(i)).ts,tracks(mavlinkTrackIndex(i)).masl,'k:','LineWidth',2)
end
for i = 1:length(radaTrackIndex)
    plot(tracks(radaTrackIndex(i)).ts,tracks(radaTrackIndex(i)).masl,'LineWidth',2)
end
xlabel('Time Sample'), ylabel('MASL [m]')

%***** DOPPLER
if contains(trackFilename,'rada')
    figure,hold on, grid
    for i = 1:length(radaTrackIndex)
        plot(tracks(radaTrackIndex(i)).ts,tracks(radaTrackIndex(i)).doppler,'LineWidth',2)
    end
    xlabel('Time sample'),ylabel('Doppler (m/s)')
end


for i = 1:length(radaTrackIndex)
    disp([epoch2date(tracks(radaTrackIndex(i)).ts(1)) ' - ' epoch2date(tracks(radaTrackIndex(i)).ts(end))])
end

% tStart = date2epoch(2021,06,23,02,00,00);
% tStop  = date2epoch(2021,06,23,05,00,00);


%***** 3D PLOT - CLEAN
figure, hold on, grid
for i = 1:length(mavlinkTrackIndex)
    plot3(tracks(mavlinkTrackIndex(i)).lon,tracks(mavlinkTrackIndex(i)).lat,tracks(mavlinkTrackIndex(i)).masl,'k:','LineWidth',2)
end

for i = 1:length(radaTrackIndex)
    plot3(tracks(radaTrackIndex(i)).lon,tracks(radaTrackIndex(i)).lat,tracks(radaTrackIndex(i)).masl,'LineWidth',2)
end
title(titleString,'FontSize',24)

%***** 2D PLOT - CLEAN
uiopen('/Users/AB/WORK-DATA/CUAS/SAT-TOPO/SAT1b-satellite-map-with-markers.fig',1)
hold on
for i = 1:length(mavlinkTrackIndex)
    plot(tracks(mavlinkTrackIndex(i)).lon,tracks(mavlinkTrackIndex(i)).lat,'k:','LineWidth',2)
end

for i = 1:length(radaTrackIndex)
    plot(tracks(radaTrackIndex(i)).lon,tracks(radaTrackIndex(i)).lat,'LineWidth',5)
end
title(titleString,'FontSize',24)


%***** MASL vs DST
figure, hold on,grid
for i = 1:length(mavlinkTrackIndex)
    plot(tracks(mavlinkTrackIndex(i)).dst/1000,tracks(mavlinkTrackIndex(i)).masl,'k:','LineWidth',2)
end
for i = 1:length(radaTrackIndex)
    plot(tracks(radaTrackIndex(i)).dst/1000,tracks(radaTrackIndex(i)).masl,'LineWidth',2)
end
xlabel('Distance [Km]'), ylabel('MASL [m]')


%***** RCS vs position
figure, hold on, grid
for i = 1:length(radaTrackIndex)
    if ~isempty(tracks(radaTrackIndex(i)).rcs)
        scatter(tracks(radaTrackIndex(i)).lon,tracks(radaTrackIndex(i)).lat,[],tracks(radaTrackIndex(i)).rcs,'filled')
    end
end
colorbar
colormap hot

%***** RCS vs range
figure, hold on, grid
for i = 1:length(radaTrackIndex)
    if ~isempty(tracks(radaTrackIndex(i)).rcs)
        plot(tracks(radaTrackIndex(i)).dst/1000,tracks(radaTrackIndex(i)).rcs,'LineWidth',2)
    end
end
xlabel('Distance [Km]'), ylabel('RCS')




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                          FUNCTIONS
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function tracks = decorateTracks(tracks,radaConnid,filterParameters)
if ~isfield(tracks(1),'connid')
    for i = 1:length(tracks)
        tracks(i).connid = radaConnid;
    end
end
for i = 1:length(tracks)
    if numel(tracks(i).lat)>1
        if isempty(tracks(i).sog)
            tracks(i).sog = sqrt(tracks(i).velocity_x.^2 + tracks(i).velocity_y.^2);
        end
        
        
        
        % CALCULATE A FEW TRACK STATISTICS
        brad = tracks(i).brg/180*pi;
        bradUnwrap = unwrap(brad);
        tracks(i).angularspeed = diff(bradUnwrap);
        tracks(i).angularspeed(end+1) = tracks(i).angularspeed(end);
        tracks(i).maxDeltaRange     = abs(max(tracks(i).dst) - min(tracks(i).dst));
        tracks(i).maxDeltaAzimuth   = abs(max(bradUnwrap)-min(bradUnwrap));
        tracks(i).diffRange         = diff(tracks(i).dst);
        tracks(i).diffAzimuth       = diff(bradUnwrap)*180/pi;
        tracks(i).stdRange          = std(diff(tracks(i).dst));
        tracks(i).stdAzimuth        = std(diff(bradUnwrap));
        %             maxDeltaRange(i)            = tracks(i).maxDeltaRange;
        %             maxDeltaAzimuth(i)          = tracks(i).maxDeltaAzimuth;
        %             diffRange(i)                = tracks(i).diffRange;
        %             diffAzimuth(i)              = tracks(i).diffAzimuth;
        %             stdRange(i)                 = tracks(i).stdRange;
        %             stdAzimuth(i)               = tracks(i).stdAzimuth;
        diffSOG = [];
        
        tracks(i).angularspeed = 0;
        tracks(i).diffSOG = [];
        tracks(i).y = [];
        tracks(i).masly = [];
        
        
        diffSOG(1) = 0;
        for j = 2:numel(tracks(i).lat)
            diffSOG(j) = abs(tracks(i).sog(j)-tracks(i).sog(j-1));
            %                 if diffSOG(j)==0
            %                     diffSOG(j) = diffSOG(j-1);
            %                 end
        end
        
        tracks(i).diffSOG = diffSOG;
        % % % %             tracks(i).diffMASL = abs(diff(tracks(i).masl));
        tracks(i).y = cusum(diffSOG,filterParameters.cusum_w);
        % % % %             tracks(i).masly = cusum(tracks(i).diffMASL,masl_cusum_w);
        % % % %             % Allow only approaching targets
        %             tracks(i).y = cusum(-tracks(i).diffRange./abs(tracks(i).diffAzimuth),cusum_w);
        % Allow all targets as soon as dr/da is large enough
        %             tracks(i).y = cusum(abs(tracks(i).diffRange)./abs(tracks(i).diffAzimuth),cusum_w);
        
    else
        tracks(i).angularspeed = 0;
        tracks(i).diffSOG = [];
        tracks(i).y = [];
        tracks(i).masly = [];
    end
end

function tracks = applyFilters(tracks,filterParameters)
% TODO: add filter on track length min
% Apply filter to all track fields (not only lon, lat, range, masl
for i = 1:length(tracks)
    
    indx1 = find(tracks(i).sog>=filterParameters.sog_max);
    indx2 = find(tracks(i).y>=filterParameters.cusum_thr);
    indx3 = find(tracks(i).masly>=filterParameters.masl_cusum_thr);
    indx4 = find(tracks(i).masl<filterParameters.masl_min);
    indx5 = find(tracks(i).sog<=filterParameters.sog_min);
    
    indx = union(indx1,indx2);
    indx = union(indx,indx3);
    indx = union(indx,indx4);
    indx = union(indx,indx5);
    
    tracks(i).lon(indx) = NaN;
    tracks(i).lat(indx) = NaN;
    tracks(i).range(indx) = NaN;
    tracks(i).masl(indx)= NaN;
    
end

function [mavlinkTrackIndex, radaTrackIndex, matchID] = mavlinkAssociation(tracks,rada,dt,refConnid,maslMin,sogMin,distThr,distSog)
mavlinkTrackIndex = [];
matchID = [];
radaTrackIndex = [];

for i = 1:length(tracks)
    if strcmp(tracks(i).connid,refConnid)
        mavlinkTrackIndex(end+1) = i;
    end
end


% Interpolate every dt seconds
for i = 1:length(tracks)
    if numel(tracks(i).ts)>1
        tracks(i).tsi = floor(tracks(i).ts(1)):dt:floor(tracks(i).ts(end));
        tracks(i).lati = interp1(tracks(i).ts,tracks(i).lat,tracks(i).tsi,'linear','extrap');
        tracks(i).loni = interp1(tracks(i).ts,tracks(i).lon,tracks(i).tsi,'linear','extrap');
        tracks(i).masli = interp1(tracks(i).ts,tracks(i).masl,tracks(i).tsi,'linear','extrap');
        tracks(i).sogi = interp1(tracks(i).ts,tracks(i).sog,tracks(i).tsi,'linear','extrap');
        tracks(i).brgi = interp1(tracks(i).ts,tracks(i).brg,tracks(i).tsi,'linear','extrap');
        tracks(i).dsti = interp1(tracks(i).ts,tracks(i).dst,tracks(i).tsi,'linear','extrap');
    end
end
% Nearest neighbour association
for i = 1:length(tracks)
    if strcmp(tracks(i).connid,refConnid)
        for j = 1:length(tracks)
            if strcmp(tracks(j).connid,rada.connid)
                if (mean(tracks(j).masl)>=maslMin) && (mean(tracks(j).sog)>=sogMin)
                    [meanDistError, meanMaslError, meanSogError, meanBrgError, brgError, maslError, tiltError, b2, radaLength,iStart1,iStop1,iStart2,iStop2] = track_distance(tracks(i),tracks(j),rada.lat,rada.lon);
                    if meanDistError <= distThr && meanSogError <= distSog
                        matchID(end+1).refID = i;
                        matchID(end).radaID = j;
                        matchID(end).meanDistError = meanDistError;
                        matchID(end).meanMaslError = meanMaslError;
                        matchID(end).meanSogError  = meanSogError;
                        matchID(end).meanBrgError  = meanBrgError;
                        matchID(end).brgError      = brgError;
                        matchID(end).maslError     = maslError;
                        matchID(end).tiltError     = tiltError;
                        matchID(end).b2      = b2;
                        matchID(end).radaLength    = radaLength;
                        matchID(end).iStart1       = iStart1;
                        matchID(end).iStop1        = iStop1;
                        matchID(end).iStart2       = iStart2;
                        matchID(end).iStop2        = iStop2;
                    end
                end
            end
            
        end
    end
end

for i = 1:length(matchID)
    radaTrackIndex(i) = matchID(i).radaID;
end

