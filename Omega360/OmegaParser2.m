function tracks = OmegaParser2(filename, vlat_thresh, vlng_thresh)
%%%%%% TO BE DISCUSSED
%     radarLat, radarLon, trackTimeOrder,  filepath , removeDuplicates
%
%% Eyeballing graphs from trial data set:
%                  -  vlat_thresh = 1.6e-3
%                  -  vlng_thresh = 2.5e-3
% *******  THIS NEEDS TO BE AUTOMATED **************

%% ******** INPUTS
% filename

%           string containing the full path of the file containing the
%           data, usually in csv format
% radarLat
%           latitude of radar in decimal degrees
% radarLon
%           longitude of radar in decimal degrees
% maxLines
%           max number of file lines to be parsed (useful incase of very
%           large files); can be equal to Inf if we want to read the full
%           file
% trackTimeOrder
%           0: do not sort by time; 1: sort by time (default)
% removeDuplicates
%           0: do not remove duplicates; 1: remove duplicates
%
%********  OUTPUT
%   structure containing tracks

% % Output Structure Data columns: 
% 1- id 
% 2- timestamp
% % 3- timestamp O
% 4- Code 
% 5- SOG
% 6- ConnDevice
% 7- ConnClass
% 8- Device Class
% 9- brg
% 10- lng (longitude)
% 11- ptid
% 12- rot 
% 13- classification 
% 14- dst
% 15- detail 
% 16- rcs 
% 17- score
% 18- status
% 19- tkref
% 20- tkid
% 21- threat
% 22- connID
% 23- lat 
% 24- class
% 25- me 
% 26- hits
% 27- ms 
% 28- cpa
% 29- age 
% 30- masl 
% 31- mh
% 32- cog

%********** CODE

%addpath(filepath)
[path,name,~] = fileparts(filename);

struct_init = table2struct(readtable(filename));

[~,index] = sortrows({struct_init.tkid}.');
struct_init = struct_init(index);
clear index

tkidUnique = unique({struct_init.tkid});
tkidAll = {struct_init.tkid};

fields = fieldnames(struct_init)';
emptycell{length(tkidUnique),length(fields)} = {};
tracks = cell2struct(emptycell, fields', 2);

clear fields emptycell

for ii = 1:length(tkidUnique)
    waitbar(ii/length(tkidUnique))
    indx = strfind(tkidAll, tkidUnique{ii});
%     indx = int32((indx-1)/32+1);
    minindx = find(~cellfun(@isempty, indx), 1 );
    maxindx = find(~cellfun(@isempty, indx), 1, 'last' );
    
    lat              = cell2mat({struct_init(minindx:maxindx).lat});
    lng              = cell2mat({struct_init(minindx:maxindx).lng});
    
    difflat          = diff(lat);
    difflng          = diff(lng);
    
    switch vlat_thresh && vlng_thresh
        case sum(abs(difflat) > vlat_thresh) >= 1 %|| 
            keep = abs(difflat) < vlat_thresh;
        case sum(abs(difflng) >  vlng_thresh) >= 1
            keep = abs(difflng) <  vlng_thresh;
        case sum(abs(difflat) > vlat_thresh) < 1 && sum(abs(difflng) >  vlng_thresh) < 1
            keep = logical(1:length(lat));
    end
        tkid             = struct_init(minindx:maxindx).tkid;
        tsO              = {struct_init(minindx:maxindx).tsO};
        ts               = {struct_init(minindx:maxindx).ts};
        connclass        = struct_init(minindx:maxindx).connclass;
        connid           = struct_init(minindx:maxindx).connid;
        classification   = struct_init(minindx:maxindx).classification;
        deviceClass      = struct_init(minindx:maxindx).deviceClass;
        lat              = cell2mat({struct_init(minindx:maxindx).lat});
        lng              = cell2mat({struct_init(minindx:maxindx).lng});
        masl             = {struct_init(minindx:maxindx).masl};
        cog              = {struct_init(minindx:maxindx).cog};
        sog              = {struct_init(minindx:maxindx).sog};
        class            = struct_init(minindx:maxindx).class;
        detail           = struct_init(minindx:maxindx).detail;
        threat           = {struct_init(minindx:maxindx).threat};
        brg              = {struct_init(minindx:maxindx).brg};
        dst              = {struct_init(minindx:maxindx).dst};
        rcs              = {struct_init(minindx:maxindx).rcs};
        id               = {struct_init(minindx:maxindx).id};
        tcpa             = {struct_init(minindx:maxindx).tcpa};
        code             = {struct_init(minindx:maxindx).code};
        conndevice       = {struct_init(minindx:maxindx).conndevice};
        ptid             = {struct_init(minindx:maxindx).ptid};
        rot              = {struct_init(minindx:maxindx).rot};
        score            = {struct_init(minindx:maxindx).score};
        status           = {struct_init(minindx:maxindx).status};
        tkref            = {struct_init(minindx:maxindx).tkref};
        me               = {struct_init(minindx:maxindx).me};
        hits             = {struct_init(minindx:maxindx).hits};
        ms               = {struct_init(minindx:maxindx).ms};
        age              = {struct_init(minindx:maxindx).age};
        cpa              = {struct_init(minindx:maxindx).cpa};
        mh               = {struct_init(minindx:maxindx).mh};
        
        
        
        if keep(end) == 0
            keep = logical([1:length(keep),0]);
        elseif keep(1) == 0
            keep = logical([0, 1:length(keep)]);
        end
        






        tracks(ii).tkid             = tkid;
        tracks(ii).tsO              = tsO(keep);
        tracks(ii).ts               = ts(keep);
        tracks(ii).connclass        = connclass;
        tracks(ii).connid           = connid;
        tracks(ii).classification   = classification;
        tracks(ii).deviceClass      = deviceClass;
        tracks(ii).lat              = lat(keep);
        tracks(ii).lng              = lng(keep);
        tracks(ii).difflat          = diff(tracks(ii).lat);
        tracks(ii).difflng          = diff(tracks(ii).lng);
        tracks(ii).masl             = masl(keep);
        tracks(ii).cog              = cog(keep);
        tracks(ii).sog              = sog(keep);
        tracks(ii).class            = class;
        tracks(ii).detail           = detail;
        tracks(ii).threat           = threat(keep);
        tracks(ii).brg              = brg(keep);
        tracks(ii).dst              = dst(keep);
        tracks(ii).rcs              = rcs(keep);
        tracks(ii).id               = id(keep);
        tracks(ii).tcpa             = tcpa(keep);
        tracks(ii).code             = code(keep);
        tracks(ii).conndevice       = conndevice(keep);
        tracks(ii).ptid             = ptid(keep);
        tracks(ii).rot              = rot(keep);
        tracks(ii).score            = score(keep);
        tracks(ii).status           = status(keep);
        tracks(ii).tkref            = tkref(keep);
        tracks(ii).me               = me(keep);
        tracks(ii).hits             = hits(keep);
        tracks(ii).ms               = ms(keep);
        tracks(ii).age              = age(keep);
        tracks(ii).cpa              = cpa(keep);
        tracks(ii).mh               = mh(keep);
end
%         [tracks(ii).bdifflat, tracks(ii).tfdifflat] = rmoutliers(tracks(ii).difflat);
%         [tracks(ii).bdifflng, tracks(ii).tfdifflng] = rmoutliers(tracks(ii).difflng);

%         if removeDuplicates
%             [~,ia] = unique(tracks(ii).ts);
%             if length(ia)~=length(tracks(ii).ts)
%                 tracks(ii).tsO = tracks(ii).tsO(ia);
%                 tracks(ii).lat = tracks(ii).lat(ia);
%                 tracks(ii).lng = tracks(ii).lng(ia);
%                 tracks(ii).masl = tracks(ii).masl(ia);
%                 tracks(ii).cog = tracks(ii).cog(ia);
%                 tracks(ii).sog = tracks(ii).sog(ia);
%                 tracks(ii).brg = tracks(ii).brg(ia);
%                 tracks(ii).dst = tracks(ii).dst(ia);
%             end
%         end
%end

display(['Data time window: ' tracks(1).ts ' to ' tracks(end).ts ])

%****** SAVE TRACKS TO FILE 
save(fullfile(path,name),'tracks')

%****** PLOT TRACKS
main = figure();
ax1 = figure();
ax2 = figure();
% difflng = [];
% difflat = [];
for ii = 1:length(tracks)
    figure(main);
    plot(tracks(ii).lng,tracks(ii).lat,'LineWidth',2)
    hold on
% difflng = cat(2, difflng, tracks(ii).difflng);
% difflat = cat(2, difflat, tracks(ii).difflat);
    figure(ax1);
    plot(tracks(ii).difflat)
    hold on
    figure(ax2);
    plot(tracks(ii).difflng)
    hold on   
end

                        