function tracks = OmegaParser(filename)
%%%%%% TO BE DISCUSSED
%%      radarLat, radarLon, trackTimeOrder,  filepath , removeDuplicates


%******** INPUTS
% filename
%           string containing the full path of the file containing the
%           data, usually in csv format
% radarLat
%           latitude of radar in decimal degrees
% radarLon
%           longitude of radar in decimal degrees
% maxLines
%           max number of file lines to be parsed (useful incase of very
%           large files); can be equal to Inf if we want to read the full
%           file
% trackTimeOrder
%           0: do not sort by time; 1: sort by time (default)
% removeDuplicates
%           0: do not remove duplicates; 1: remove duplicates
%
%********  OUTPUT
%   structure containing tracks

% % Output Structure Data columns:
% 1- id
% 2- timestamp
% % 3- timestamp O
% 4- Code
% 5- SOG
% 6- ConnDevice
% 7- ConnClass
% 8- Device Class
% 9- brg
% 10- lng (longitude)
% 11- ptid
% 12- rot
% 13- classification
% 14- dst
% 15- detail
% 16- rcs
% 17- score
% 18- status
% 19- tkref
% 20- tkid
% 21- threat
% 22- connID
% 23- lat
% 24- class
% 25- me
% 26- hits
% 27- ms
% 28- cpa
% 29- age
% 30- masl
% 31- mh
% 32- cog

%********** CODE

%addpath(filepath)
[path,name,~] = fileparts(filename);

struct_init = table2struct(readtable(filename));

[~,index] = sortrows({struct_init.tkid}.');
struct_init = struct_init(index);
clear index

tkidUnique = unique({struct_init.tkid});
tkidAll = {struct_init.tkid};
tkidAll = [tkidAll{:}];

for ii = 1:length(tkidUnique)
    waitbar(ii/length(tkidUnique))
    indx = strfind(tkidAll, tkidUnique{ii});
    indx = int32((indx-1)/32+1);
    minindx = indx(1);
    maxindx = indx(end);
    %     minindx = find(~cellfun(@isempty, indx), 1 );
    %     maxindx = find(~cellfun(@isempty, indx), 1, 'last' );
    tracks(ii).tkid             = {struct_init(minindx:maxindx).tkid};
    tracks(ii).tkref             = cell2mat({struct_init(minindx:maxindx).tkref});
    if  isfield(struct_init,'tso')
        tracks(ii).tso              = cell2mat({struct_init(minindx:maxindx).tso});
    else
        tracks(ii).ts              = cell2mat({struct_init(minindx:maxindx).tsO});
    end
    tracks(ii).ts              = {struct_init(minindx:maxindx).ts};
    if  isfield(struct_init,'connclass')
        tracks(ii).connclass        = struct_init(minindx:maxindx).connclass;
    else
        tracks(ii).connlass        = struct_init(minindx:maxindx).connClass;
    end
    tracks(ii).connid           = struct_init(minindx:maxindx).connid;
    tracks(ii).classification   = struct_init(minindx:maxindx).classification;
    tracks(ii).deviceclass      = struct_init(minindx:maxindx).deviceClass;
    tracks(ii).lat              = cell2mat({struct_init(minindx:maxindx).lat});
    tracks(ii).lng              = cell2mat({struct_init(minindx:maxindx).lng});
    tracks(ii).masl             = cell2mat({struct_init(minindx:maxindx).masl});
    tracks(ii).cog              = cell2mat({struct_init(minindx:maxindx).cog});
    tracks(ii).sog              = cell2mat({struct_init(minindx:maxindx).sog});
    tracks(ii).class            = struct_init(minindx:maxindx).class;
    tracks(ii).detail           = {struct_init(minindx:maxindx).detail};
    tracks(ii).threat           = {struct_init(minindx:maxindx).threat};
    tracks(ii).brg              = cell2mat({struct_init(minindx:maxindx).brg});
    tracks(ii).dst              = cell2mat({struct_init(minindx:maxindx).dst});
    tracks(ii).rcs              = cell2mat({struct_init(minindx:maxindx).rcs});
    tracks(ii).status           = cell2mat({struct_init(minindx:maxindx).status});
    
    
    
    %         if removeDuplicates
    %             [~,ia] = unique(tracks(ii).ts);
    %             if length(ia)~=length(tracks(ii).ts)
    %                 tracks(ii).tsO = tracks(ii).tsO(ia);
    %                 tracks(ii).lat = tracks(ii).lat(ia);
    %                 tracks(ii).lng = tracks(ii).lng(ia);
    %                 tracks(ii).masl = tracks(ii).masl(ia);
    %                 tracks(ii).cog = tracks(ii).cog(ia);
    %                 tracks(ii).sog = tracks(ii).sog(ia);
    %                 tracks(ii).brg = tracks(ii).brg(ia);
    %                 tracks(ii).dst = tracks(ii).dst(ia);
    %             end
    %         end
end

%display(['Data time window: ' tracks(1).ts{1} ' to ' tracks(end).ts{end} ])

%****** SAVE TRACKS TO FILE
save(fullfile(path,name),'tracks')

%****** PLOT TRACKS
figure,hold on
for i = 1:length(tracks)
    %      plot(tracks(i).lng,tracks(i).lat,'LineWidth',2)
    
    indx = strfind(tracks(i).status,'T');
    lng = tracks(i).lng(indx);
    lat = tracks(i).lat(indx);
    plot(lng,lat,'LineWidth',2)
    %
    %     indx = strfind(tracks(i).status,'N');
    %     lng = tracks(i).lng(indx);
    %     lat = tracks(i).lat(indx);
    %     plot(lng,lat,'k*')
    %
    %     indx = strfind(tracks(i).status,'L');
    %     lng = tracks(i).lng(indx);
    %     lat = tracks(i).lat(indx);
    %     plot(lng,lat,'r*')
    
    %     indx = strfind(tracks(i).status,'L');
    %     lng = tracks(i).lng;
    %     lat = tracks(i).lat;
    %     lng(indx) = [];
    %     lat(indx) = [];
    %     plot(lng,lat,'LineWidth',2)
end

