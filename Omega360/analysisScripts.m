for i = 1:length(tracks)
    trackLength(i) = length(tracks(i).lat);
end

figure,hold on
for i = 1:length(tracks)
    if length(tracks(i).lat)>10
        for j = 1:length(tracks(i).lat)
            switch tracks(i).detail{j}
                case '{"family": "GENERIC"}'
                    markerColor = 'g';
                case '{"family": "DRONE"}'
                    markerColor = 'm';
                case '{"family": "SHIP"}'
                    markerColor = 'y';
                case '{"family": "AIRCRAFT"}'
                    markerColor = 'b';
                case '{"family": "VEHICLE"}'
                    markerColor = 'c';
                case '{"family": "DRONE_MICRO"}'
                    markerColor = 'r';
            end
            plot(tracks(i).lng(j),tracks(i).lat(j),[markerColor '*'])
        end
    end
end

figure,hold on
droneTrackIndx = [];
for i = 1:length(tracks)
%    if any(strcmp(tracks(i).detail,'{"family": "DRONE"}')) || any(strcmp(tracks(i).detail,'{"family": "DRONE_MICRO"}'))
    if  any(strcmp(tracks(i).detail,'{"family": "DRONE_MICRO"}'))
        lng = tracks(i).lng;
        lat = tracks(i).lat;
        
        plot(lng,lat,'k')
        
        indx = strfind(tracks(i).status,'L');
        lng(indx) = NaN;
        lat(indx) = NaN;
        indx = strfind(tracks(i).status,'N');
        lng(indx) = NaN;
        lat(indx) = NaN;
        
        plot(lng,lat,'LineWidth',2)
        
        indx = find(strcmp(tracks(i).detail,'{"family": "DRONE"}'));
        plot(lng(indx),lat(indx),'+m')
        
        indx = find(strcmp(tracks(i).detail,'{"family": "DRONE_MICRO"}'));
        plot(lng(indx),lat(indx),'or')
        
        droneTrackIndx = [droneTrackIndx i];
    end
end



figure,hold on
for i = 1:length(tracks)
    lng = tracks(i).lng;
    lat = tracks(i).lat;
    
    %         plot(lng,lat,'k')
    text(lng(1),lat(1),num2str(i))
    
    
    indx = strfind(tracks(i).status,'T');
    lng = lng(indx);
    lat = lat(indx);
    plot(lng,lat,'LineWidth',2)
    
    plot(lng,lat,'LineWidth',2)
end






TKREF = [];
for i = 1:length(tracks)
    TKREF = [TKREF tracks(i).tkref];
end
figure,plot(TKREF)